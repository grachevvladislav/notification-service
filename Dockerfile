FROM python:3.12.2-slim

WORKDIR /app
COPY requirements.txt /app/
RUN pip3 install -r /app/requirements.txt --no-cache-dir
COPY notification_service /app
