import os

from celery import Celery
from celery.schedules import crontab
from django.conf import settings  # noqa

os.environ.setdefault(
    "DJANGO_SETTINGS_MODULE", "notification_service.settings"
)
app = Celery("notification_service")
app.config_from_object("django.conf:settings", namespace="CELERY")
app.autodiscover_tasks()


app.conf.beat_schedule = {
    "get_categories_every_one_minutes": {
        "task": "core.main.tasks.get_messages",
        "schedule": crontab(minute="*/1"),
    },
}
