from datetime import datetime, timedelta, timezone

import requests
from celery.schedules import crontab
from django.db.models import F
from rest_framework.status import HTTP_200_OK

from notification_service import settings
from notification_service.celery import app

from .models import Message
from .utils import IntervalByHours

app.conf.beat_schedule = {
    "get_categories_every_one_minutes": {
        "task": "core.tasks.get_messages",
        "schedule": crontab(minute="*/1"),
    },
}


@app.task
def get_messages():
    """Adding messages to the queue that can already be sent."""
    print("get_messages_started")
    messages = Message.objects.filter(
        distribution__start_time__lte=datetime.now(timezone.utc)
        + IntervalByHours("client__timezone"),
        status=Message.Status.CREATED,
    ).order_by(
        F("distribution__start_time") + IntervalByHours("client__timezone")
    )
    for message in messages:
        message.status = Message.Status.IN_QUEUE
    Message.objects.bulk_update(messages, ["status"])
    for message in messages:
        data = {
            "id": message.id,
            "phone": message.client.phone,
            "text": message.distribution.text,
        }
        utc_end_time = message.distribution.end_time - timedelta(
            hours=message.client.timezone
        )
        message.status, message.time = send_message(data, utc_end_time)
        message.save()


@app.task
def send_message(data, utc_end_time):
    print("send_message_started")
    url = "https://probe.fbrq.cloud/v1/send/{}".format(data["id"])
    headers = {
        "Accept": "application/json",
        "Authorization": "Bearer {}".format(settings.EXTERNAL_API_TOKEN),
        "Content-Type": "application/json",
    }
    if utc_end_time < datetime.now(timezone.utc):
        return Message.Status.OVERDUE, None
    response = requests.post(url=url, headers=headers, json=data)
    if response.status_code == HTTP_200_OK:
        return Message.Status.SENT, datetime.now(timezone.utc)
    else:
        return Message.Status.CREATED, None
