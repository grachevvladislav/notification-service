from rest_framework.exceptions import ValidationError

WRONG_PHONE_NUMBER = (
    "Номер телефона должен быть в формате " "7XXXXXXXXXX (X - цифра от 0 до 9)"
)
WRONG_PHONE_CODE = (
    "Допустимо использование телефонных кодов Российских "
    "операторов. Коды от 900 до 997"
)
WRONG_TIMEZONE = "Допустимо число от -12 до 14"


def phone_validator(value):
    """Phone number matches a pattern 7XXXXXXXXXX (X - number from 0 to 9)."""
    if value not in range(7 * 10**10, 8 * 10**10 - 1):
        raise ValidationError(WRONG_PHONE_NUMBER)
    return value


def phone_code_validator(value):
    """Phone code between 900 and 997."""
    if value not in range(900, 997):
        raise ValidationError(WRONG_PHONE_CODE)
    return value


def timezone_validator(value):
    """Timezone between 900 and 997."""
    if value > 14 or value < -12:
        raise ValidationError(WRONG_TIMEZONE)
    return value
