from django.urls import include, path
from rest_framework.routers import DefaultRouter

from .views import ClientViewSet, DistributionViewSet

router_v1 = DefaultRouter()
router_v1.register("client", ClientViewSet, basename="client")
router_v1.register(
    "distribution", DistributionViewSet, basename="distribution"
)


urlpatterns = [
    path("v1/", include(router_v1.urls)),
]
