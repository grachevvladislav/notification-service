from datetime import datetime, timezone

from rest_framework import serializers

from .models import Client, Distribution, Message
from .utils import set_if_not_none


class ClientSerializer(serializers.ModelSerializer):
    """Client model serializer."""

    class Meta:
        """Metaclass client model serializer."""

        model = Client
        fields = (
            "id",
            "mobile_code",
            "phone",
            "timezone",
            "tag",
        )
        optional_fields = ("tag",)
        read_only_fields = ("id", "mobile_code")

    def create(self, validated_data):
        """Create mobile_code filed from validated data and save."""
        phone = validated_data["phone"]
        validated_data["mobile_code"] = phone // 10**7 % 10**3
        client = Client.objects.create(**validated_data)
        return client


class DistributionSerializer(serializers.ModelSerializer):
    """Distribution model serializer."""

    def validate(self, data):
        """Validate data."""
        if data["start_time"] > data["end_time"]:
            raise serializers.ValidationError(
                "Рассылка не может заканчиваться раньше её начала!"
            )
        return data

    def create(self, validated_data):
        """Create distribution object."""
        distribution = Distribution.objects.create(**validated_data)
        filters = {}
        set_if_not_none(filters, "mobile_code", distribution.mobile_code)
        set_if_not_none(filters, "tag", distribution.tag)
        clients = Client.objects.filter(**filters)

        for client in clients:
            Message.objects.create(distribution=distribution, client=client)
        return distribution

    def update(self, instance, validated_data):
        """Update distribution object."""
        instance.text = validated_data.get("text", instance.text)
        instance.start_time = validated_data.get(
            "start_time", instance.start_time
        )
        if "end_time" in validated_data:
            if validated_data["end_time"] <= datetime.now(timezone.utc):
                Message.objects.filter(status=Message.Status.CREATED).update(
                    status=Message.Status.OVERDUE
                )
            elif (
                validated_data["end_time"]
                > datetime.now(timezone.utc)
                > instance.end_time
            ):
                Message.objects.filter(status=Message.Status.OVERDUE).update(
                    status=Message.Status.CREATED
                )
            instance.end_time = validated_data["end_time"]
        filters = {}
        set_if_not_none(
            filters, "mobile_code", validated_data.get("mobile_code", None)
        )
        set_if_not_none(filters, "tag", validated_data.get("tag", None))
        if filters:
            Message.objects.filter(
                distribution=instance, status=Message.Status.CREATED
            ).delete()
            clients = Client.objects.filter(**filters)
            for client in clients:
                Message.objects.create(distribution=instance, client=client)
        instance.save()
        return instance

    class Meta:
        """Metaclass distribution model serializer."""

        model = Distribution
        fields = (
            "id",
            "start_time",
            "end_time",
            "text",
            "tag",
            "mobile_code",
        )
        optional_fields = (
            "tag",
            "mobile_code",
        )
        read_only_fields = ("id",)
