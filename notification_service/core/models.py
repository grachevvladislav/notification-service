from django.db import models

from .validators import (
    phone_code_validator,
    phone_validator,
    timezone_validator,
)


class Distribution(models.Model):
    """Distribution model."""

    start_time = models.DateTimeField()
    end_time = models.DateTimeField()
    text = models.CharField(max_length=500)
    mobile_code = models.IntegerField(null=True)
    tag = models.CharField(max_length=200, blank=True)


class Client(models.Model):
    """Client model."""

    phone = models.BigIntegerField(unique=True, validators=[phone_validator])
    mobile_code = models.IntegerField(validators=[phone_code_validator])
    tag = models.CharField(max_length=200, blank=True)
    timezone = models.FloatField(validators=[timezone_validator])


class Message(models.Model):
    """Message model."""

    class Status(models.TextChoices):
        """Types of status."""

        CREATED = "CREATED", "Создано"
        IN_QUEUE = "IN QUEUE", "В очереди"
        SENT = "SENT", "Отправлено"
        OVERDUE = "OVERDUE", "Просрочено"

    time = models.DateTimeField(blank=True, null=True)
    status = models.CharField(
        max_length=9, choices=Status.choices, default=Status.CREATED
    )
    distribution = models.ForeignKey(
        Distribution, on_delete=models.CASCADE, related_name="messages"
    )
    client = models.ForeignKey(
        Client, on_delete=models.CASCADE, related_name="messages"
    )
