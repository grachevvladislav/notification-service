# Generated by Django 5.0.2 on 2024-02-25 17:26

import core.validators
import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = []

    operations = [
        migrations.CreateModel(
            name="Client",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "phone",
                    models.BigIntegerField(
                        unique=True,
                        validators=[core.validators.phone_validator],
                    ),
                ),
                (
                    "mobile_code",
                    models.IntegerField(
                        validators=[core.validators.phone_code_validator]
                    ),
                ),
                ("tag", models.CharField(blank=True, max_length=200)),
                (
                    "timezone",
                    models.FloatField(
                        validators=[core.validators.timezone_validator]
                    ),
                ),
            ],
        ),
        migrations.CreateModel(
            name="Distribution",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("start_time", models.DateTimeField()),
                ("end_time", models.DateTimeField()),
                ("text", models.CharField(max_length=500)),
                ("mobile_code", models.IntegerField(null=True)),
                ("tag", models.CharField(blank=True, max_length=200)),
            ],
        ),
        migrations.CreateModel(
            name="Message",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("time", models.DateTimeField(blank=True, null=True)),
                (
                    "status",
                    models.CharField(
                        choices=[
                            ("CREATED", "Создано"),
                            ("IN QUEUE", "В очереди"),
                            ("SENT", "Отправлено"),
                            ("OVERDUE", "Просрочено"),
                        ],
                        default="CREATED",
                        max_length=9,
                    ),
                ),
                (
                    "client",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="messages",
                        to="core.client",
                    ),
                ),
                (
                    "distribution",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="messages",
                        to="core.distribution",
                    ),
                ),
            ],
        ),
    ]
