from rest_framework import mixins, viewsets

from .models import Client, Distribution
from .serializers import ClientSerializer, DistributionSerializer


class ClientViewSet(
    mixins.CreateModelMixin,
    mixins.UpdateModelMixin,
    mixins.DestroyModelMixin,
    viewsets.GenericViewSet,
):
    """Client view set."""

    queryset = Client.objects.all()
    serializer_class = ClientSerializer


class DistributionViewSet(
    mixins.CreateModelMixin,
    mixins.UpdateModelMixin,
    mixins.DestroyModelMixin,
    mixins.ListModelMixin,
    mixins.RetrieveModelMixin,
    viewsets.GenericViewSet,
):
    """Distribution view set."""

    queryset = Distribution.objects.all()
    serializer_class = DistributionSerializer
