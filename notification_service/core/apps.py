from django.apps import AppConfig


class CoreConfig(AppConfig):
    """Configuration class."""

    default_auto_field = "django.db.models.BigAutoField"
    name = "core"
