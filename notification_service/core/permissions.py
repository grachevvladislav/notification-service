from rest_framework import permissions


class IsAuthenticated(permissions.BasePermission):
    """Permission class for api."""

    def has_permission(self, request, view):
        """Available for authenticated users."""
        return request.user.is_authenticated
