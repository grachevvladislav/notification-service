from django.db.models import Func


def set_if_not_none(mapping, key, value):
    if value is not None:
        mapping[key] = value


class IntervalByHours(Func):
    """SQL INTERVAL by horre."""

    function = "INTERVAL"
    template = "(%(expressions)s * %(function)s '1' HOUR)"
